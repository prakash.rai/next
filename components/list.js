import React from 'react'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

const list = (props) => {
    


  return (
    <div key={props.key}>
    <div className={styles.box1 + " mb-3"}>
      <div className="row">
        <div className="col-4">
          <img src={props.flags} width="100%" height="90%" />
        </div>
        <div className="col">
          <h2>{props.name}</h2>
          <p className="m-0">Currency : {props.curren}</p>
          <p className="m-0 mb-1">Current date and time :{props.time} </p>
          <a className='btn btn-outline-primary' style={{ 'width': '48%' }} href={props.maps}>Show Map</a>
          <Link href={"/detail/" + props.nxt}><a className='btn btn-outline-primary' style={{ 'width': '48%', 'marginLeft': '10px' }}>Detail</a></Link>
        </div>
      </div>
    </div>
  </div>
    
  )
}

export default list;
